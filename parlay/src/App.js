import Recognizer from "./Recognizer"
import React from 'react'
import './App.css'


const Parlay = ({ results }) => <div className="App-header"> {
	[...results].map(r => [r[0]].map(({ confidence, transcript }) => 
		<p 
			onClick={window.scrollTo(0,document.body.scrollHeight)} 
			style={{color:r.isFinal ? 'white' : '#61dafb' }}
			onDoubleClick={()=> {
				[...r].map(({ confidence: c, transcript: t })=> console.log(`${t} (${Math.round(c*100)}%)`))
				console.log('')
			}}
		>{ `${transcript} ${r.isFinal ? `(${Math.round(confidence*100)}%)` : ''} ` }</p>		
	))	
} {	[...Array(2 - [...results].filter(r => !r.isFinal).length)].map(_ => <p style={{color: '#282c34'}}> No Content </p>) }</div>


export default Recognizer(Parlay)
